package com.etezaz.nfccard.ui.writeNfc.adapter;


import android.content.Context;
import android.view.Gravity;

import com.etezaz.nfccard.R;
import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.ui.writeNfc.tableviewModel.CellModel;
import com.etezaz.nfccard.ui.writeNfc.tableviewModel.ColumnHeaderModel;
import com.etezaz.nfccard.ui.writeNfc.tableviewModel.RowHeaderModel;

import java.util.ArrayList;
import java.util.List;

public class MyTableViewModel {
    // View Types
    public static final int ACTIONS_TYPE = 1;
    public static final int MONEY_TYPE = 2;

    private List<ColumnHeaderModel> mColumnHeaderModelList;
    private List<RowHeaderModel> mRowHeaderModelList;
    private List<List<CellModel>> mCellModelList;
    private Context context;
    public MyTableViewModel(Context context)
    {
            this.context=context;
    }

    public int getCellItemViewType(int column) {

        switch (column) {
            case 3:
                // 5. column header is gender.
                return ACTIONS_TYPE;
            case 2:
                // 8. column header is Salary.
                return MONEY_TYPE;
            default:
                return 0;
        }
    }


    public int getColumnTextAlign(int column) {
        switch (column) {
            // Id
            case 0:
                return Gravity.CENTER;
            // Name
            case 1:
                return Gravity.CENTER;
            // Nickname
            case 2:
                return Gravity.CENTER;
            // Email
            case 3:
                return Gravity.START;
            // BirthDay
            case 4:
                return Gravity.CENTER;
            // Gender (Sex)
            case 5:
                return Gravity.CENTER;
            // Age
            case 6:
                return Gravity.CENTER;
            // Job
            case 7:
                return Gravity.CENTER;
            // Salary
            case 8:
                return Gravity.CENTER;
            // CreatedAt
            case 9:
                return Gravity.CENTER;
            // UpdatedAt
            case 10:
                return Gravity.CENTER;
            // Address
            case 11:
                return Gravity.LEFT;
            // Zip Code
            case 12:
                return Gravity.RIGHT;
            // Phone
            case 13:
                return Gravity.RIGHT;
            // Fax
            case 14:
                return Gravity.RIGHT;
            default:
                return Gravity.CENTER;
        }

    }

    private List<ColumnHeaderModel> createColumnHeaderModelList() {
        List<ColumnHeaderModel> list = new ArrayList<>();

        // Create Column Headers
       // list.add(new ColumnHeaderModel(""));
        list.add(new ColumnHeaderModel(context.getResources().getString(R.string.transNo)));
        list.add(new ColumnHeaderModel(context.getResources().getString(R.string.balanceNo)));
        list.add(new ColumnHeaderModel(context.getResources().getString(R.string.oldUsd)));
        list.add(new ColumnHeaderModel(context.getResources().getString(R.string.newUsd)));
        list.add(new ColumnHeaderModel(context.getResources().getString(R.string.newEur)));
        list.add(new ColumnHeaderModel(context.getResources().getString(R.string.exchangeRate)));

        return list;
    }

    private List<List<CellModel>> createCellModelList(List<Balance> balance) {
        List<List<CellModel>> lists = new ArrayList<>();

        // Creating cell model list from BalanceAction list for Cell Items
        // In this example, BalanceAction list is populated from web service

        for (int i = 0; i < balance.size(); i++) {
            Balance balance1 = balance.get(i);

            List<CellModel> list = new ArrayList<>();

            // The order should be same with column header list;
         //   list.add(new CellModel("1-" + i, i+1)); // "id"
            list.add(new CellModel("1-" + i, balance1.getId()));
            list.add(new CellModel("2-" + i, balance1.getBalanceNo()));
            list.add(new CellModel("3-" + i, balance1.getOldBalanceUSD()));
            list.add(new CellModel("4-" + i,balance1.getNewBalanceUSD()));
            list.add(new CellModel("5-" + i,balance1.getNewBalanceEUR()));
            list.add(new CellModel("6-" + i,balance1.getExchangeRate()));

            // Add
            lists.add(list);
        }

        return lists;
    }

    private List<RowHeaderModel> createRowHeaderList(int size) {
        List<RowHeaderModel> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            // In this example, Row headers just shows the index of the TableView List.
           list.add(new RowHeaderModel(String.valueOf(i + 1)));
        }
        return list;
    }


    public List<ColumnHeaderModel> getColumHeaderModeList() {
        return mColumnHeaderModelList;
    }

    public List<RowHeaderModel> getRowHeaderModelList() {
        return mRowHeaderModelList;
    }

    public List<List<CellModel>> getCellModelList() {
        return mCellModelList;
    }

    public void generateListForTableView(List<Balance> balance) {
        mColumnHeaderModelList = createColumnHeaderModelList();
        mCellModelList = createCellModelList(balance);
        mRowHeaderModelList = createRowHeaderList(balance.size());
    }

}



