package com.etezaz.nfccard.ui.writeNfc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import com.etezaz.nfccard.R;
import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.model.response.RetroBalance;
import com.etezaz.nfccard.presenter.BalanceWritePresenter;
import com.etezaz.nfccard.ui.writeNfc.adapter.MyTableAdapter;
import com.etezaz.nfccard.ui.writeNfc.adapter.MyTableViewListener;
import com.etezaz.nfccard.view.BalanceWriteView;
import com.evrencoskun.tableview.TableView;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class WriteActivity extends AppCompatActivity implements BalanceWriteView {

    private int value1=10,value2=20;

    //region TableView
    private MyTableAdapter mTableAdapter;
    private TableView mTableView;
    private int page=1;
    private float xDpi;
    //endregion

    private BalanceWritePresenter balancePresenter;
    ProgressDialog progressDoalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_main);

        mTableView = findViewById(R.id.my_TableView);

        initializeTableView(mTableView);

        balancePresenter = new BalanceWritePresenter(this, getApplicationContext());

        progressDoalog = new ProgressDialog(WriteActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();

        balancePresenter.getBalance();

    }

    private void initializeTableView(TableView tableView){

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        // these will return the actual dpi  vertically
        xDpi = dm.widthPixels;
        // Create TableView Adapter
        mTableAdapter = new MyTableAdapter(getApplicationContext(),((int) (xDpi/6.29)));
        tableView.setAdapter(mTableAdapter);

    }

    @Override
    public void displayBalance(List<Balance> balanceList) {
        // Create TableView Adapter
        mTableAdapter.setBalance(balanceList);
        progressDoalog.cancel();
    }

    @Override
    public void getBalance(RetroBalance retroBalance) {
        Intent intent=getIntent();

        Balance balance=new Balance();
        balance.setBalanceNo(intent.getLongExtra("balanceNo",0));
        balance.setOldBalanceUSD(intent.getLongExtra("oldBalanceUSD",0));
        balance.setNewBalanceUSD(intent.getLongExtra("newBalanceUSD",0));
        balance.setNewBalanceEUR(0);
        balance.setExchangeRate(retroBalance.getRates().getUSD());

        balancePresenter.insertBalanceToDB(balance);
        balancePresenter.getAllBalance();
    }

    @Override
    public void displayErrorMessage() {

    }
}