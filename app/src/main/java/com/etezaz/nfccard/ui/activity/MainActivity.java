package com.etezaz.nfccard.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.etezaz.nfccard.R;
import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.presenter.BalancePresenter;
import com.etezaz.nfccard.ui.writeNfc.WriteActivity;
import com.etezaz.nfccard.view.BalanceView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements BalanceView {

    private int value1=10,value2=20;
    private BalancePresenter balancePresenter;
    private int btnNo=-1;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        editor.putLong("firstValue", 10);
        editor.putLong("secondValue", 20);
        editor.apply();

        balancePresenter = new BalancePresenter(this, getApplicationContext());

        (findViewById(R.id.btnSub2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnNo=0;
                balancePresenter.getAllBalance();
            }
        });

        (findViewById(R.id.btnSub5)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnNo=1;
                balancePresenter.getAllBalance();
            }
        });

        (findViewById(R.id.btnReset)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnNo=2;
                balancePresenter.getAllBalance();

            }
        });

    }

    @Override
    public void displayBalance(List<Balance> balanceList) {
        Intent intent = new Intent(getApplicationContext(), WriteActivity.class);
        switch (btnNo)
        {
            case 0:
                intent.putExtra("balanceNo",1L);
                intent.putExtra("oldBalanceUSD",pref.getLong("firstValue", 0));
                editor.putLong("firstValue", pref.getLong("firstValue", 0)-2);
                editor.apply();
                intent.putExtra("newBalanceUSD",pref.getLong("firstValue", 0));
                startActivity(intent);
                break;
            case 1:
                intent.putExtra("balanceNo",2L);
                intent.putExtra("oldBalanceUSD",pref.getLong("secondValue", 0));
                editor.putLong("secondValue", pref.getLong("secondValue", 0)-5);
                editor.apply();
                intent.putExtra("newBalanceUSD",pref.getLong("secondValue", 0));
                startActivity(intent);
                break;
            case 2:
                pref.getLong("firstValue", 10);
                pref.getLong("secondValue", 20);
                editor.apply();
                break;
        }
    }
}