package com.etezaz.nfccard.ui.writeNfc.tableviewModel;

public class    RowHeaderModel {
    private String mData;

    public RowHeaderModel(String mData) {
        this.mData = mData;
    }

    public String getData() {
        return mData;
    }
}
