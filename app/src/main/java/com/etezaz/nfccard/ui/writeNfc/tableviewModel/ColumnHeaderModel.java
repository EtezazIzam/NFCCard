package com.etezaz.nfccard.ui.writeNfc.tableviewModel;


public class ColumnHeaderModel {

    private String mData;

    public ColumnHeaderModel(String mData) {
        this.mData = mData;
    }

    public String getData() {
        return mData;
    }
}
