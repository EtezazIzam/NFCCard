package com.etezaz.nfccard.view;


import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.model.response.RetroBalance;

import java.util.List;

public interface BalanceWriteView {

    void displayBalance(List<Balance> balanceList);
    void getBalance(RetroBalance retroBalance);
    void displayErrorMessage();
    
}
