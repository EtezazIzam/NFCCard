package com.etezaz.nfccard.view;


import com.etezaz.nfccard.model.db.Balance;

import java.util.List;

public interface BalanceView {

    void displayBalance(List<Balance> balanceList);

}
