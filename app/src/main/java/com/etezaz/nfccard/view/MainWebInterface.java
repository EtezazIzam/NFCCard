package com.etezaz.nfccard.view;

import com.etezaz.nfccard.model.response.RetroBalance;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MainWebInterface {

    @GET("/latest")
    Call<RetroBalance> getAllBalances();
}
