package com.etezaz.nfccard.model.model_helper;


import android.database.sqlite.SQLiteDatabase;

import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.model.db.dao.BalanceDao;
import com.etezaz.nfccard.model.db.dao.DaoMaster;
import com.etezaz.nfccard.model.db.dao.DaoSession;

import java.util.List;

public class BalanceModel {

    private com.etezaz.nfccard.model.db.dao.BalanceDao BalanceDao;

    public BalanceModel(DaoMaster.DevOpenHelper openHelper) {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        DaoMaster  master = new DaoMaster(db);//create masterDao
        DaoSession daoSession = master.newSession(); //Creates Session session
        this.BalanceDao = daoSession.getBalanceDao();
    }

    public List<Balance> getAllBalance(){
        return BalanceDao.queryBuilder().
                orderDesc(com.etezaz.nfccard.model.db.dao.BalanceDao.Properties.Id).build().list();
    }

    public void deleteBalance(Long BalanceId) {
        BalanceDao.deleteByKey(BalanceId);
    }

    public void insertBalanceToDB(Balance balance){
        BalanceDao.insertOrReplace(balance);
    }

}
