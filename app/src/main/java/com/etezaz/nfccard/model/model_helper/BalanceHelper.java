package com.etezaz.nfccard.model.model_helper;


import android.content.Context;

import com.etezaz.nfccard.model.api.RetrofitClientInstance;
import com.etezaz.nfccard.model.response.RetroBalance;
import com.etezaz.nfccard.presenter.BalanceWritePresenter;
import com.etezaz.nfccard.view.MainWebInterface;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BalanceHelper implements Callback<RetroBalance> {

    private Context context;
    private BalanceWritePresenter balancePresenter;
    public BalanceHelper(Context context)
    {
        this.context = context;
    }

    public BalanceHelper setPresenter(BalanceWritePresenter balancePresenter)
    {
        this.balancePresenter = balancePresenter;
        return this;
    }

    public void loadBalance()
    {
        MainWebInterface service = RetrofitClientInstance.getRetrofitInstance().create(MainWebInterface.class);
        Call<RetroBalance> call = service.getAllBalances();
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<RetroBalance> call, Response<RetroBalance> response) {
        if (response.isSuccessful()) {
            RetroBalance retroBalance = response.body();
            showBalance(retroBalance);
        }
        else
        {
            showNoConnectionMessage();
        }
    }

    @Override
    public void onFailure(Call<RetroBalance> call, Throwable t) {
        showNoConnectionMessage();
    }

    private void showNoConnectionMessage() {
        if (balancePresenter != null)
            balancePresenter.showNoConnectionMessage();
    }

    private void showBalance(RetroBalance retroBalance) {
        if (balancePresenter != null)
            balancePresenter.showBalance(retroBalance);
    }
}
