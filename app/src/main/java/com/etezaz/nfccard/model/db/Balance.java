package com.etezaz.nfccard.model.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

@Entity
public class Balance {

    @Id
    private Long id;

    private long balanceNo;
    private long oldBalanceUSD;
    private long newBalanceUSD;
    private long newBalanceEUR;
    private double exchangeRate;
    @Generated(hash = 1710033858)
    public Balance(Long id, long balanceNo, long oldBalanceUSD, long newBalanceUSD,
            long newBalanceEUR, double exchangeRate) {
        this.id = id;
        this.balanceNo = balanceNo;
        this.oldBalanceUSD = oldBalanceUSD;
        this.newBalanceUSD = newBalanceUSD;
        this.newBalanceEUR = newBalanceEUR;
        this.exchangeRate = exchangeRate;
    }
    @Generated(hash = 942283129)
    public Balance() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public long getBalanceNo() {
        return this.balanceNo;
    }
    public void setBalanceNo(long balanceNo) {
        this.balanceNo = balanceNo;
    }
    public long getOldBalanceUSD() {
        return this.oldBalanceUSD;
    }
    public void setOldBalanceUSD(long oldBalanceUSD) {
        this.oldBalanceUSD = oldBalanceUSD;
    }
    public long getNewBalanceUSD() {
        return this.newBalanceUSD;
    }
    public void setNewBalanceUSD(long newBalanceUSD) {
        this.newBalanceUSD = newBalanceUSD;
    }
    public long getNewBalanceEUR() {
        return this.newBalanceEUR;
    }
    public void setNewBalanceEUR(long newBalanceEUR) {
        this.newBalanceEUR = newBalanceEUR;
    }
    public double getExchangeRate() {
        return this.exchangeRate;
    }
    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    
  }