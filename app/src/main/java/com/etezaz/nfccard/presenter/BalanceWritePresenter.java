package com.etezaz.nfccard.presenter;


import android.content.Context;

import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.model.db.dao.DaoMaster;
import com.etezaz.nfccard.model.model_helper.BalanceHelper;
import com.etezaz.nfccard.model.model_helper.BalanceModel;
import com.etezaz.nfccard.model.response.RetroBalance;
import com.etezaz.nfccard.view.BalanceWriteView;

import java.util.List;

public class BalanceWritePresenter {

    private Context context;
    private BalanceWriteView balanceView;
    private DaoMaster.DevOpenHelper openHelper;
    private BalanceHelper balanceHelper;

    public BalanceWritePresenter(BalanceWriteView balanceView, Context context) {
        this.openHelper = new DaoMaster.DevOpenHelper(context, "Balance_db", null);
        this.context=context;
        this.balanceView =balanceView;
        balanceHelper = new BalanceHelper(context).setPresenter(this);
    }
    
    //Network
    public void getBalance()
    {
        balanceHelper.loadBalance();
    }

    //View
    public void showBalance(RetroBalance retroBalance)
    {
        balanceView.getBalance(retroBalance);
    }

    public void showNoConnectionMessage()
    {
        balanceView.displayErrorMessage();
    }

    public void getAllBalance(){
        BalanceModel BalanceModel = new BalanceModel(openHelper);
        List<Balance> balanceList = BalanceModel.getAllBalance();
        balanceView.displayBalance(balanceList);
    }


    public void insertBalanceToDB(Balance notepad){
        BalanceModel notepadModel = new BalanceModel(openHelper);
        notepadModel.insertBalanceToDB(notepad);
    }
    
}
