package com.etezaz.nfccard.presenter;


import android.content.Context;

import com.etezaz.nfccard.model.db.Balance;
import com.etezaz.nfccard.model.db.dao.DaoMaster;
import com.etezaz.nfccard.model.model_helper.BalanceHelper;
import com.etezaz.nfccard.model.model_helper.BalanceModel;
import com.etezaz.nfccard.model.response.RetroBalance;
import com.etezaz.nfccard.view.BalanceView;

import java.util.List;

public class BalancePresenter {

    private Context context;
    private com.etezaz.nfccard.view.BalanceView balanceView;
    private DaoMaster.DevOpenHelper openHelper;

    public BalancePresenter(BalanceView balanceView, Context context) {
        this.openHelper = new DaoMaster.DevOpenHelper(context, "Balance_db", null);
        this.context=context;
        this.balanceView =balanceView;
    }

    public void getAllBalance(){
        BalanceModel BalanceModel = new BalanceModel(openHelper);
        List<Balance> balanceList = BalanceModel.getAllBalance();
        balanceView.displayBalance(balanceList);
    }


    public void insertBalanceToDB(Balance notepad){
        BalanceModel notepadModel = new BalanceModel(openHelper);
        notepadModel.insertBalanceToDB(notepad);
    }
    
}
